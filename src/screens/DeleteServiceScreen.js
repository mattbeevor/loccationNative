import React, { Component } from 'react';
import {View, StyleSheet, Text, TouchableHighlight} from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

import { connect } from 'react-redux';
import * as actions from '../actions';


class DeleteServiceScreen extends Component {

  _delete(){
    this.props.deleteService(this.props.navigation.state.params.service.key)
    this.props.navigation.navigate('MyServices');
  }

  render() {
    return (
      <TouchableHighlight underlayColor={"#BBBBFF"} style={styles.item} onPress={()=>{this._delete()}}>
        <Text>Confirm permanently remove Service: {this.props.navigation.state.params.service.nm}</Text>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  item: {
		height:50,
		borderBottomWidth:1,
		borderBottomColor: "#AAAAAA",
		backgroundColor:"#EEEEFF"
	},
  Button: {
    height:50,
    borderBottomWidth:1,
    borderBottomColor: "#AAAAAA",
    backgroundColor:"#EEEEFF"
  },
  text:{
    fontSize:25,
  }
});

const mapStateToProps = (state) => {
	return {
    linked: state.ServiceReducer.linked,
	};
}

//export default connect(actions)(DeleteLayerScreen);
export default connect(mapStateToProps,actions)(DeleteServiceScreen);
