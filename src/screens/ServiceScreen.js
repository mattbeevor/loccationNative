import React, { Component } from 'react';
import Colours from '../constants/Colours';
import ServiceList from './ServiceList';
import ScanButton from '../components/ScanButton'

import { View, ToastAndroid} from 'react-native';

import { connect } from 'react-redux';
import * as actions from '../actions';

class ServiceScreen extends Component {

  componentDidUpdate(){
    if(this.props.currentmessage){
      ToastAndroid.show(this.props.currentmessage, ToastAndroid.SHORT);
      this.props.message("")
    }
    console.log("help me")
  }



  render() {
    return (
      <View>
        <ScanButton />
        <ServiceList />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
	return {
		currentmessage: state.ServiceReducer.message,
	};
}

export default connect(mapStateToProps,actions)(ServiceScreen);
