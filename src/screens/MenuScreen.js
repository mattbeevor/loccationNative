import React, { Component } from 'react';
import { TouchableOpacity, ScrollView, View, StyleSheet, Text, Image } from 'react-native';
import Colours from '../constants/Colours';
import Login from '../components/Login'

class MenuScreen extends Component {
  render() {
    return (
        <View >
            <Login/>
            <TouchableOpacity style={styles.Button}
            onPress={() => this.props.navigation.navigate('MyServices')}>
              <Text>My Services</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.Button}
            onPress={() => this.props.navigation.navigate('MyLayers')}>
              <Text>My Layers</Text>
            </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  Button: {
    height:50,
    borderBottomWidth:1,
    borderBottomColor: "#AAAAAA",
    backgroundColor:"#EEEEFF"
  },
  text:{
    fontSize:25,
  }
});

export default MenuScreen;
