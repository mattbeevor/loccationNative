import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

import MyLayersList from './MyLayersList';

class MyLayersScreen extends Component {



  render() {
    return (
      <View>
        <TouchableOpacity
        onPress={() => this.props.navigation.navigate('CreateLayer')}>
          <Text>+ Create Layer</Text>
        </TouchableOpacity>
        <MyLayersList navigation={this.props.navigation}/>
      </View>
    );
  }
}

export default MyLayersScreen;
