import React, { Component,  } from 'react';
import {ScrollView, TouchableHighlight, SectionList, Text, View, Dimensions, Image, StyleSheet } from 'react-native';
import { List, ListItem, Button, Icon, } from 'react-native-elements';
import Colours from '../constants/Colours';

import { connect } from 'react-redux';
import * as actions from '../actions';

var { height, width } = Dimensions.get('window');

class LayerList extends Component {



  selectlayer = (index,id) => {
    console.log("SELECT",id)
    this.props.selectlayer(index)
    if(this.props.logged_in){
      this.props.subscribe(id)
    }
  }

  unselectlayer = (index,id) => {
    this.props.deselectlayer(index)
    if(this.props.logged_in){
      this.props.unsubscribe(id)
    }
  }



	//selecteditem = ({ item, index, section: { title, data } }) => {return <ListItem onPress={()=>{this.unselectlayer(index)}} key={index} title={index+item.key+item.nm} badge={{ value: "On", textStyle: { color: 'green' } }}/>}
	//availableitem = ({ item, index, section: { title, data } }) => {return <ListItem onPress={()=>{this.selectlayer(index)}} key={index} title={index+item.key+item.nm} badge={{ value: "Off", textStyle: { color: 'orange' }}}/>}

	render () {

    let list=this.props.my_layers.map((layer,index)=>
       <TouchableHighlight underlayColor={'white'} style={styles.my} key={layer.key} onPress={()=>{this.unselectlayer(index,layer.key)}}>
      <Text style={styles.listText} key={layer.key}>{layer.nm} key:{layer.key} index:{index}</Text>
      </TouchableHighlight>
    )

    let list2=this.props.nearby_layers.map((layer,index)=>
       <TouchableHighlight underlayColor={"#99CC99"} style={styles.near} key={layer.key} onPress={()=>{this.selectlayer(index,layer.key)}}>
        <Text style={styles.listText} key={layer.key}>{layer.nm}key{layer.key}index{index}</Text>
      </TouchableHighlight>
    )

		return (
			<ScrollView>{list}{list2}</ScrollView>

		);
	}
};

const styles = StyleSheet.create({
  my: {
    backgroundColor:"#99CC99",
    height:50,
    borderBottomWidth :1,
    borderBottomColor: '#777777',
  },
	near: {
    height:50,
    borderBottomWidth :1,
    borderBottomColor: '#777777',
	},
  listText:{
    fontSize:16,
  }
});

const mapStateToProps = (state) => {
	return {
    nearby_layers: state.ServiceReducer.nearby_layers,
    my_layers: state.ServiceReducer.my_layers,
    logged_in: state.AuthReducer.logged_in
	};
}


export default connect(mapStateToProps, actions)(LayerList);
