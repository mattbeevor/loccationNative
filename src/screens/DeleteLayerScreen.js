import React, { Component } from 'react';
import {View, StyleSheet, Text, TouchableHighlight} from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

import { connect } from 'react-redux';
import * as actions from '../actions';


class DeleteLayerScreen extends Component {

  _delete(){
    this.props.deleteLayer(this.props.navigation.state.params.layer.key)
    this.props.navigation.navigate('MyLayers');
  }

  render() {
    return (
      <TouchableHighlight underlayColor={"#BBBBFF"} style={styles.item} onPress={()=>{this._delete()}}>
        <Text>Confirm permanently remove Layer: {this.props.navigation.state.params.layer.nm}</Text>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  item: {
		height:50,
		borderBottomWidth:1,
		borderBottomColor: "#AAAAAA",
		backgroundColor:"#EEEEFF"
	},
  Button: {
    height:50,
    borderBottomWidth:1,
    borderBottomColor: "#AAAAAA",
    backgroundColor:"#EEEEFF"
  },
  text:{
    fontSize:25,
  }
});

const mapStateToProps = (state) => {
	return {
    linked: state.ServiceReducer.linked,
	};
}

//export default connect(actions)(DeleteLayerScreen);
export default connect(mapStateToProps,actions)(DeleteLayerScreen);
