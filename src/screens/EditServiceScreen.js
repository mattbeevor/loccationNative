import React, { Component } from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

import { connect } from 'react-redux';
import * as actions from '../actions';


class EditServiceScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
    name: this.props.navigation.state.params.service.nm,
    description: this.props.navigation.state.params.service.ds,
   };
  }

  update = () => {
    this.props.updateService(this.props.navigation.state.params.service.key,this.state.name,this.state.description)
    this.props.navigation.navigate('MyServices');
  }

  render() {

    return (
      <View >
      <FormLabel>Name</FormLabel>
      <FormInput onChangeText={(text) => this.setState({name:text})}/>
      <FormValidationMessage>Error message</FormValidationMessage>
      <FormLabel>Description</FormLabel>
      <FormInput onChangeText={(text) => this.setState({description:text})}/>
      <FormValidationMessage>Error message</FormValidationMessage>
      <TouchableOpacity style={styles.Button}
      onPress={this.update} >
      <Text style={styles.text}>Confirm</Text>
      </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Button: {
    height:50,
    borderBottomWidth:1,
    borderBottomColor: "#AAAAAA",
    backgroundColor:"#EEEEFF"
  },
  text:{
    fontSize:25,
  }
});

const mapStateToProps = (state) => {
	return {
    linked: state.ServiceReducer.linked,
	};
}

export default connect(mapStateToProps, actions)(EditServiceScreen);
