import React, { Component } from 'react';
import {Dimensions, View,ScrollView, StyleSheet, Text, TouchableOpacity, CheckBox} from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

import { connect } from 'react-redux';
import * as actions from '../actions';

var { height, width } = Dimensions.get('window');

class CreateServiceScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
    name: "",
    description: "",
    link: "https://www.",
    lat:"",
    long:"",
    live:false,
    here:true,
    manual:false,
   };
  }

  setLocCurrent=(value)=>{
    this.setState({live:!value})
    this.setState({here:value})
    this.setState({manual:false})
  }

  setLocLive=(value)=>{
    this.setState({live:value})
    this.setState({here:!value})
    this.setState({manual:false})
  }

  setLocManual=(value)=>{
    this.setState({live:false})
    this.setState({here:!value})
    this.setState({manual:value})
  }

  create = () => {
    let lat
    let long
    if(this.state.manual){
      lat=this.state.lat
      long=this.state.long
    }else{
      lat=this.props.current_location[0]
      long=this.props.current_location[1]
    }

    this.props.createService(this.state.name,this.state.description,lat,long,this.state.live,this.state.link)
    this.props.navigation.navigate('MyServices');
  }

  render() {

    return (
      <ScrollView >
      <FormLabel>Name</FormLabel>
      <FormInput onChangeText={(text) => this.setState({name:text})}/>
      <FormLabel>Description</FormLabel>
      <FormInput onChangeText={(text) => this.setState({description:text})}/>
      <FormLabel>Link</FormLabel>
      <FormInput onChangeText={(text) => this.setState({link:text})}/>

      <Text>Location</Text>
      <Text>Dynamic service, follow this device</Text><CheckBox value={this.state.live} onValueChange={(value) => this.setLocLive(value)}/>
      <Text>Static service at current location</Text><CheckBox value={this.state.here} onValueChange={(value) => this.setLocCurrent(value)}/>
      <Text>Static service at other location</Text><CheckBox value={this.state.manual} onValueChange={(value) => this.setLocManual(value)}/>


      { this.state.manual &&
        <View>
      <Text >Coordinates:</Text>
      <FormLabel >Latitude</FormLabel>
      <FormInput onChangeText={(text) => this.setState({lat:text})} />
      <FormLabel >Longitude</FormLabel>
      <FormInput onChangeText={(text) => this.setState({long:text})} />
      </View>
      }

      <TouchableOpacity style={styles.Button}
      onPress={this.create} >
      <Text style={styles.text}>Create</Text>
      </TouchableOpacity>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  Button: {
    height:50,
    borderBottomWidth:1,
    borderBottomColor: "#AAAAAA",
    backgroundColor:"#EEEEFF"
  },
  text:{
    fontSize:25,
  }
});

const mapStateToProps = (state) => {
	return {
    current_location: state.ServiceReducer.current_location,
    linked: state.ServiceReducer.linked,
	};
}

//export default connect(actions)(DeleteLayerScreen);
export default connect(mapStateToProps,actions)(CreateServiceScreen);
