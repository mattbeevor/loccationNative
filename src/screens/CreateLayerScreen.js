import React, { Component } from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

import { connect } from 'react-redux';
import * as actions from '../actions';


class CreateLayerScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
    name: "",
    description: "",
   };
  }

  create = () => {
    this.props.createLayer(this.state.name,this.state.description)
    this.props.navigation.navigate('MyLayers');
  }

  render() {

    return (
      <View >
      <FormLabel>Name</FormLabel>
      <FormInput onChangeText={(text) => this.setState({name:text})}/>
      <FormLabel>Description</FormLabel>
      <FormInput onChangeText={(text) => this.setState({description:text})}/>
      <TouchableOpacity style={styles.Button}
      onPress={this.create} >
      <Text style={styles.text}>Create</Text>
      </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Button: {
    height:50,
    borderBottomWidth:1,
    borderBottomColor: "#AAAAAA",
    backgroundColor:"#EEEEFF"
  },
  text:{
    fontSize:25,
  }
});

const mapStateToProps = (state) => {
	return {
    linked: state.ServiceReducer.linked,
	};
}

//export default connect(actions)(DeleteLayerScreen);
export default connect(mapStateToProps,actions)(CreateLayerScreen);
