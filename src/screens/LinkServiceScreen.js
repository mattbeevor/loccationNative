import React, { Component } from 'react';
import {ScrollView ,View, StyleSheet, Text, TouchableHighlight} from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

import { connect } from 'react-redux';
import * as actions from '../actions';
import unionBy from 'lodash.unionby';
import differenceBy from 'lodash.differenceby';


class LinkServiceScreen extends Component {

  componentDidMount(){
    this.props.clearlinks()
    this.props.getLinkedLayers(this.props.navigation.state.params.service.key)
  }

  _link(layer){
    this.props.createListing(layer,this.props.navigation.state.params.service,"service")
  }

  _unlink(layer){
    this.props.removeListing(layer,this.props.navigation.state.params.service,"service")
  }

  render() {

    const layers = unionBy(this.props.created_layers,this.props.my_layers.concat(this.props.nearby_layers),'key');
    const unselected = differenceBy(layers,this.props.linked,'key');

    const list=this.props.linked.map((layer)=>
      <TouchableHighlight underlayColor={"#BBFFBB"} style={styles.item1} key={layer.key} onPress={()=>{this._unlink(layer)}}>
        <Text style={styles.listText} key={layer.key}>{layer.nm}</Text>
      </TouchableHighlight>
    )
    const list2=unselected.map((layer)=>
      <TouchableHighlight underlayColor={"#BBBBFF"} style={styles.item2} key={layer.key} onPress={()=>{this._link(layer)}}>
        <Text style={styles.listText} key={layer.key}>{layer.nm}</Text>
      </TouchableHighlight>
    )

    return (
      <ScrollView>
      {list}
      {list2}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  item1: {
    height:50,
    borderBottomWidth:1,
    borderBottomColor: "#AAAAAA",
    backgroundColor:"#EEEEFF"
  },
  item2: {
      height:50,
      borderBottomWidth:1,
      borderBottomColor: "#AAAAAA",
      backgroundColor:"#FFEEEE"
    },
  text:{
    fontSize:25,
  },
  listText:{
    fontSize:16,
  }
});

const mapStateToProps = (state) => {
	return {
    created_layers: state.ServiceReducer.created_layers,
    my_layers: state.ServiceReducer.my_layers,
    nearby_layers: state.ServiceReducer.nearby_layers,
    linked: state.ServiceReducer.linked,
	};
}

export default connect(mapStateToProps, actions)(LinkServiceScreen);
