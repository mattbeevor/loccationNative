import React, { Component } from 'react';
import {ScrollView ,View, StyleSheet, Text, TouchableHighlight} from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

import { connect } from 'react-redux';
import * as actions from '../actions';
import unionBy from 'lodash.unionby';
import differenceBy from 'lodash.differenceby';


class LinkLayerScreen extends Component {

  componentDidMount(){
    this.props.clearlinks()
    this.props.getLinkedServices(this.props.navigation.state.params.layer.key)
  }

  _link(service){
    this.props.createListing(this.props.navigation.state.params.layer,service,"layer")
  }

  _unlink(service){
    this.props.removeListing(this.props.navigation.state.params.layer,service,"layer")
  }

  render() {

    const services = unionBy(this.props.created_services,this.props.scanned_services,'key');
    const unselected = differenceBy(services,this.props.linked,'key');

    const list=this.props.linked.map((service)=>
      <TouchableHighlight underlayColor={"#BBFFBB"} style={styles.item1} key={service.key} onPress={()=>{this._unlink(service)}}>
        <Text style={styles.listText} key={service.key}>{service.nm}</Text>
      </TouchableHighlight>
    )
    const list2=unselected.map((service)=>
      <TouchableHighlight underlayColor={"#BBBBFF"} style={styles.item2} key={service.key} onPress={()=>{this._link(service)}}>
        <Text style={styles.listText} key={service.key}>{service.nm}</Text>
      </TouchableHighlight>
    )

    return (
      <ScrollView>
      {list}
      {list2}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  item1: {
    height:50,
    borderBottomWidth:1,
    borderBottomColor: "#AAAAAA",
    backgroundColor:"#EEEEFF"
  },
  item2: {
      height:50,
      borderBottomWidth:1,
      borderBottomColor: "#AAAAAA",
      backgroundColor:"#FFEEEE"
    },
  text:{
    fontSize:25,
  },
  listText:{
    fontSize:16,
  }
});

const mapStateToProps = (state) => {
	return {
    created_services: state.ServiceReducer.created_services,
    scanned_services: state.ServiceReducer.scanned_services,
    linked: state.ServiceReducer.linked,
	};
}

export default connect(mapStateToProps, actions)(LinkLayerScreen);
