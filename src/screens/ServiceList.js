import React, { Component } from 'react';
import { Linking, RefreshControl, Button, Text, View, AsyncStorage, Dimensions, Image, ScrollView, StyleSheet, TouchableHighlight } from 'react-native';
import { Icon } from 'react-native-elements';
import Colours from '../constants/Colours';

import { connect } from 'react-redux';
import * as actions from '../actions';

var { height, width } = Dimensions.get('window');

class ServiceList extends Component {

	doscan = () => {
    if(this.props.logged_in){
      this.props.userscan()
    }else{
      let selected=this.props.my_layers.map(layer=>layer.key)

			console.log("SELECTED",selected)
      this.props.publicscan(selected)
    }
  }

	link=(url)=>{Linking.openURL(url)}

	render () {

		const list=this.props.scanned_services.map((service)=>
      <TouchableHighlight underlayColor={"#BBBBFF"} style={styles.item} key={service.key} onPress={()=>{this.link(service.lk)}}>
        <Text style={styles.listText} key={service.key}>{service.nm} - {Math.round(service.distance * 10) / 10} metres</Text>
      </TouchableHighlight>
    )

		return (
			<ScrollView style={{height: height-30}}
			refreshControl={
			          <RefreshControl
									refreshing={this.props.is_scanning}
			            onRefresh={this.doscan}
			          />
			        }
			>

			{list}
		  </ScrollView>
		);
	}
};

const styles = StyleSheet.create({
	item: {
		height:50,
		borderBottomWidth:1,
		borderBottomColor: "#AAAAAA",
		backgroundColor:"#EEEEFF"
	},
	listText:{
		fontSize:16,
	}
});

const mapStateToProps = (state) => {
	return {
		scanned_services: state.ServiceReducer.scanned_services,
		is_scanning: state.ServiceReducer.is_scanning,
		my_layers: state.ServiceReducer.my_layers,
		logged_in: state.AuthReducer.logged_in
	};
}

export default connect(mapStateToProps, actions)(ServiceList);
