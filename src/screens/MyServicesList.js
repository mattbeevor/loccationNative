import React, { Component } from 'react';
import {Dimensions, Text, ScrollView, TouchableHighlight, Modal, StyleSheet, View } from 'react-native';
import Colours from '../constants/Colours';

import { connect } from 'react-redux';
import * as actions from '../actions';

var { height, width } = Dimensions.get('window');


class MyServicesList extends Component {

  constructor(props) {
    super(props);
    this.state = {serviceSelected:"",modalVisible:false};
  }

  componentDidMount(){
    this.props.getMyServices()
  }



  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  selectService(service) {
    this.setModalVisible(true);
    this.setState({serviceSelected: service});
  }

	render () {
    console.log("Hello",this.props.created_services)

		const list=this.props.created_services.map((service)=>
      <TouchableHighlight underlayColor={"#BBBBFF"} style={styles.item} key={service.key} onPress={() => {
            this.selectService(service);}}>
        <Text style={styles.listText} key={service.key}>{service.nm}</Text>
      </TouchableHighlight>
    )

		return (

      <View>

			<ScrollView style={{height: height-100}}>
			{list}
		  </ScrollView>

      <View style={{marginTop: 22}}>
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
        <View style={{marginTop: 22}}>
          <View>

          <TouchableHighlight style={styles.item}
            onPress={() => {
              this.setModalVisible(!this.state.modalVisible);
              this.props.navigation.navigate('LinkService',{service:this.state.serviceSelected});
            }}>
            <Text> Add Layers to Service {this.state.serviceSelected.key}</Text>
          </TouchableHighlight>

            <TouchableHighlight style={styles.item}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
                this.props.navigation.navigate('EditService',{service:this.state.serviceSelected});
              }}>
              <Text>Edit Service</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.item}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
                this.props.navigation.navigate('DeleteService',{service:this.state.serviceSelected});
              }}>
              <Text>Delete Service</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.item}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}>
              <Text>Cancel</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      </View>

      </View>
		);
	}
};

const styles = StyleSheet.create({
	item: {
		height:50,
		borderBottomWidth:1,
		borderBottomColor: "#AAAAAA",
		backgroundColor:"#EEEEFF"
	},
	listText:{
		fontSize:16,
	}
});

const mapStateToProps = (state) => {
	return {
		created_services: state.ServiceReducer.created_services,
	};
}

export default connect(mapStateToProps, actions)(MyServicesList);
