import React, { Component } from 'react';
import {Dimensions, Text, ScrollView, TouchableHighlight, Modal, StyleSheet, View } from 'react-native';
import Colours from '../constants/Colours';

import { connect } from 'react-redux';
import * as actions from '../actions';

var { height, width } = Dimensions.get('window');


class MyLayersList extends Component {

  constructor(props) {
    super(props);
    this.state = {layerSelected:"",modalVisible:false};
  }

  componentDidMount(){
    this.props.getMyLayers()
  }



  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  selectLayer(layer) {
    this.setModalVisible(true);
    this.setState({layerSelected: layer});
  }

	render () {
    console.log("Hello",this.props.created_layers)

		const list=this.props.created_layers.map((layer)=>
      <TouchableHighlight underlayColor={"#BBBBFF"} style={styles.item} key={layer.key} onPress={() => {
            this.selectLayer(layer);}}>
        <Text style={styles.listText} key={layer.key}>{layer.nm}</Text>
      </TouchableHighlight>
    )

		return (

      <View>

			<ScrollView style={{height: height-100}}>
			{list}
		  </ScrollView>

      <View style={{marginTop: 22}}>
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
                    console.log('Modal has been closed.');
                  }}
        >

        <View style={{marginTop: 22}}>
          <View>

          <TouchableHighlight style={styles.item}
            onPress={() => {
              this.setModalVisible(!this.state.modalVisible);
              this.props.navigation.navigate('LinkLayer',{layer:this.state.layerSelected});
            }}>
            <Text> Add Services to Layer {this.state.layerSelected.key}</Text>
          </TouchableHighlight>

            <TouchableHighlight style={styles.item}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
                this.props.navigation.navigate('EditLayer',{layer:this.state.layerSelected});
              }}>
              <Text>Edit Layer</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.item}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
                this.props.navigation.navigate('DeleteLayer',{layer:this.state.layerSelected});
              }}>
              <Text>Delete Layer</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.item}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}>
              <Text>Cancel</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      </View>

      </View>
		);
	}
};

const styles = StyleSheet.create({
	item: {
		height:50,
		borderBottomWidth:1,
		borderBottomColor: "#AAAAAA",
		backgroundColor:"#EEEEFF"
	},
	listText:{
		fontSize:16,
	}
});

const mapStateToProps = (state) => {
	return {
		created_layers: state.ServiceReducer.created_layers,
	};
}

export default connect(mapStateToProps, actions)(MyLayersList);
