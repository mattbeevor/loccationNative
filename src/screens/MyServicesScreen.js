import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

import MyServicesList from './MyServicesList';

class MyServicesScreen extends Component {

  render() {
    return (
      <View>
        <TouchableOpacity
        onPress={() => this.props.navigation.navigate('CreateService')}>
          <Text>+ Create Service</Text>
        </TouchableOpacity>
        <MyServicesList navigation={this.props.navigation}/>
      </View>
    );
  }
}

export default MyServicesScreen;
