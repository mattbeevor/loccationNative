import MenuScreen from './MenuScreen';
import MyServicesScreen from './MyServicesScreen';
import MyLayersScreen from './MyLayersScreen';
import LinkLayerScreen from './LinkLayerScreen';
import DeleteLayerScreen from './DeleteLayerScreen';
import EditLayerScreen from './EditLayerScreen';
import CreateLayerScreen from './CreateLayerScreen';
import LinkServiceScreen from './LinkServiceScreen';
import DeleteServiceScreen from './DeleteServiceScreen';
import EditServiceScreen from './EditServiceScreen';
import CreateServiceScreen from './CreateServiceScreen';


import {
  createStackNavigator,
} from 'react-navigation';



const MenuStack = createStackNavigator({
  Home: {screen:MenuScreen,header:null},
  MyLayers:  {screen:MyLayersScreen,header:null},
  LinkLayer:  {screen:LinkLayerScreen,header:null},
  DeleteLayer:  {screen:DeleteLayerScreen,header:null},
  EditLayer:  {screen:EditLayerScreen,header:null},
  CreateLayer:  {screen:CreateLayerScreen,header:"Create Layer"},
  MyServices:  {screen:MyServicesScreen,header:null}, //\1
  LinkService:  {screen:LinkServiceScreen,header:null}, //2
  DeleteService:  {screen:DeleteServiceScreen,header:null}, //3
  EditService:  {screen:EditServiceScreen,header:null}, //4
  CreateService:  {screen:CreateServiceScreen,header:"Create Service"}, //5
}, {
  initialRouteName: 'Home',
  headerMode: 'none',
});

export default MenuStack
