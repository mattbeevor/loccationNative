import { combineReducers } from 'redux';
import ServiceReducer from './ServiceReducer';
import AuthReducer from './AuthReducer';

export default combineReducers({
  ServiceReducer, AuthReducer
});
