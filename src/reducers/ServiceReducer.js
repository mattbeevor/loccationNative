import {
	RECEIVE_DATA,
	SCANNING,
	NOTSCANNING,
	SET_POSITION,
	SUBSCRIBE,
	UNSUBSCRIBE,
	SELECT_LAYER,
	DESELECT_LAYER,
	SHOW_MESSAGE,
	GET_MY_SERVICES,
	GET_MY_LAYERS,
	GET_LINKED_SERVICES,
	CLEAR_LINKS,
	LINK,
	UN_LINK,
	GET_LINKED_LAYERS,
} from '../actions';

export const INITIAL_STATE = {
  scanned_services:[],
	nearby_layers:[],
	my_layers:[{key:1,prvt:false,nm:"Public",ds:"Public Layer",locs:[0,0]}],
	my_services:[],
	linked:[],
	is_scanning: false,
	current_location: [5,5],
	message: "",
	created_layers:[],
	created_services:[],
};

//GeoDataSource code
let dist = (lat1, lon1, lat2, lon2, unit) => {
        var radlat1 = Math.PI * lat1/180
        var radlat2 = Math.PI * lat2/180
        var theta = lon1-lon2
        var radtheta = Math.PI * theta/180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180/Math.PI
        dist = dist * 60 * 1.1515
	if (unit=="M") { dist = dist * 1609.344 }
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        return dist
}

let distance = (service,pos) =>{
	lat1=pos[0]
	long1=pos[1]
	lat2=service.loc[1] //Sequelize puts them the wrong way round (long,lat)
	long2=service.loc[0]
	dis=dist(lat1, long1, lat2, long2, "M")
	return {...service, distance:dis}
}

let compare = (a,b) => {
  if (a.distance < b.distance){
    return -1
  }else{
		return 1
	}
}

let sort = (services,pos) =>{
	let s=services.map(
		function(service) { return distance(service, pos); }
	)
	return s.sort(compare)
}

let swapList=(fromList,toList,index,top)=>{
	layer=fromList.splice(index,1)
	if(top){
		toList=layer.concat(toList)
	}else{
		toList=toList.concat(layer)
	}

	return {toList:JSON.parse(JSON.stringify(toList)),
					fromList:JSON.parse(JSON.stringify(fromList))}
}

let unlink=(list,id)=>{
	var index = list.indexOf(id);
	if (index > -1) {
	  list.splice(index, 1);
	}

	return [].concat(list)
}

export default ( state=INITIAL_STATE, action ) => {
	switch (action.type) {
	/*	case SUBSCRIBE:
			return {...state, }
		case UNSUBSCRIBE:
				return {...state, }*/
		case SHOW_MESSAGE:
				return {...state, message: action.message}
		case SELECT_LAYER:
				let selected=swapList(state.nearby_layers,state.my_layers,action.id,false)
				return {...state,my_layers: selected.toList, nearby_layers: selected.fromList}
		case DESELECT_LAYER:
				let deselected=swapList(state.my_layers,state.nearby_layers,action.id,true)
				return {...state,my_layers: deselected.fromList, nearby_layers: deselected.toList}
		case SET_POSITION:
				return { ...state, is_scanning: true ,current_location: action.position}
		case RECEIVE_DATA:
				console.log(action.services.length,action.myLayers.length,action.nearbyLayers.length)
				let services=sort(action.services, action.position)
		    return {...state, scanned_services:services, nearby_layers:action.nearbyLayers, my_layers:action.myLayers, is_scanning: false };
		case SCANNING:
				return { ...state, is_scanning: true }
		case 	NOTSCANNING:
				return { ...state, is_scanning: false }
		case GET_MY_LAYERS:
				return  {...state, created_layers:action.layers}
		case GET_MY_SERVICES:
				return  {...state, created_services:action.services }
		case GET_LINKED_SERVICES:
				return  {...state, linked:action.services }
		case CLEAR_LINKS:
				return {...state, linked:[]}
		case UN_LINK:
				list=unlink(state.linked,action.item)
				return {...state, linked:list}
		case LINK:
				list=[].concat(state.linked).concat([action.item])
				return {...state, linked:list}
		case 	GET_LINKED_LAYERS:
				return  {...state, linked:action.layers }
		default:
			return state;
	}
};
