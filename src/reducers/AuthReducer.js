import {
	LOG_IN,
	LOG_OUT,
	TEST,
	AUTH_MESSAGE
} from '../actions';

export const INITIAL_STATE = {
  logged_in: false,
	message: ""
};

export default ( state=INITIAL_STATE, action ) => {
	switch (action.type) {
		case AUTH_MESSAGE:
			return { ...state, message: action.message}
		case LOG_IN:
			return { ...state, logged_in: action.value}
		case LOG_OUT:
			return { ...state, logged_in: false}
		case TEST:
				return { ...state, animals: action.value}
		default:
			return state;
	}
};
