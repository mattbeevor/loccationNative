import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, StatusBar} from 'react-native';
import { Provider } from 'react-redux';
import MainNavigator from './navigation';
import { connect } from 'react-redux';
import store from './store';
import Colours from './constants/Colours';

type Props = {};
export default class App extends Component<{}> {
  render() {
    return (
      <View style={{ flex:1 }}>
        <StatusBar barStyle="light-content" backgroundColor={Colours.barColour}/>
        <Provider store={store}>
          <MainNavigator/>
        </Provider>
      </View>
    );
  }
}
