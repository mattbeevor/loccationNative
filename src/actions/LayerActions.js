import { AsyncStorage } from 'react-native';
import { INITIAL_STATE } from '../reducers/ServiceReducer';
var axios=require('axios');
import api from '../constants/apilocation';
import {axiosError} from './ServiceActions';

export const GET_LINKED_SERVICES='get_linked_services'
export const GET_MY_LAYERS = 'get_my_layers'

export const receiveLinkedServices = (services)=>{
  return{
    services: services.data,
    type: GET_LINKED_SERVICES
  }
}

export const getLinkedServices=(layer) => async dispatch => {
   var url=api.url+"/linkedservices"
   axios.post(url,{"layer":layer})
  .then(services => dispatch(receiveLinkedServices(services)))
		.catch(error => {
		    dispatch(axiosError(error))
		 })
}

export const createLayer=(name,description) => async dispatch => {
   var url=api.url+"/createlayer"
   axios.post(url,{"name":name,"description":description})
  .then(response => dispatch(getMyLayers()))
	.catch(error => {
	    dispatch(axiosError(error))
	 })
}

export const deleteLayer=(id) => async dispatch => {
   var url=api.url+"/removelayer"
   axios.post(url,{"layer":id})
  .then(response => dispatch(getMyLayers()))
		.catch(error => {
		    dispatch(axiosError(error))
		 })
}

export const updateLayer=(id,name,description) => async dispatch => {
   var url=api.url+"/updatelayer"
   axios.post(url,{"id":id,"name":name,"description":description})
  .then(response => dispatch(getMyLayers()))
		.catch(error => {
		    dispatch(axiosError(error))
		 })
}

export const receiveMyLayers = (layers)=>{
  console.log("layers",layers.data)
  return{
    layers: layers.data,
    type: GET_MY_LAYERS
  }
}

export const getMyLayers = (id) => async dispatch => {
   var url=api.url+"/mylayers"
   axios.get(url)
  .then(response => dispatch(receiveMyLayers(response)))
	.catch(error => {
	    dispatch(axiosError(error))
	 })
}
