export const LOG_IN = 'log_in';
export const LOG_OUT = 'log_out';
export const TEST='test';
var axios=require('axios');
import api from '../constants/apilocation';

export const AUTH_MESSAGE = 'message'

export const auth_message =(message)=>{
  return{
    message: message,
    type: AUTH_MESSAGE
  }
}

const axiosError=(error)=>dispatch =>{
 if (error.response) {
   console.log("LookHERE")
   console.log(error.response.data);
   console.log(error.response.status);
   console.log(error.response.headers);
   let message="Auth request Failed (Server error)"
   if(error.response.data.error){
     if(error.response.data.error=="invalid credentials"){
       message="invalid credentials"
     }
   }
   dispatch(auth_message(message))
 } else if (error.request) {
   console.log(error.request);
   dispatch(auth_message("Auth request Failed (No response)"))
 } else {
   console.log('Error', error.message);
   dispatch(auth_message("Auth request Failed (Could not send)"))
 }
 console.log(error.config);
}



export const loggedout=()=>{
	return{
		type: LOG_OUT
	}
}

export const receiveAuthentication = (value) =>{
	return {
		type: LOG_IN,
		value: value,
	}
}



export const logout = () => async dispatch => {
	 var url=api.url+"/logout"
   axios.get(url)
	 		.then(()=>{dispatch(loggedout())
			dispatch(auth_message("Logged Out"))}
			)
			.catch(error => {
		    dispatch(axiosError(error))
			})
}


export const login = (email) => async dispatch => {
		var url=api.url+"/login"
    axios.post(url,
   	{
			"email"	: email,
			"password":"secret"
		})
		.then(response => {dispatch(receiveAuthentication(true))
			dispatch(auth_message("Logged In"))
		})
    .catch(error => {
			dispatch(receiveAuthentication(false))
     	dispatch(axiosError(error))
     })
}
