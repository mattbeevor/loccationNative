import { AsyncStorage } from 'react-native';
import { INITIAL_STATE } from '../reducers/ServiceReducer';
var axios=require('axios');
import api from '../constants/apilocation';
import {getLinkedServices} from './LayerActions';


export const RECEIVE_DATA = 'receive_data';
export const SCANNING = 'scanning';
export const NOTSCANNING = 'finishscan';
export const SET_POSITION = 'set_position'
export const SELECT_LAYER = 'select_layer'
export const DESELECT_LAYER = 'deselect_layer'
export const SUBSCRIBE = 'subscribe'
export const UNSUBSCRIBE = 'unsubscribe'
export const SHOW_MESSAGE = 'message'
export const GET_MY_SERVICES = 'get_my_services'
export const LINK = 'link'
export const UN_LINK = 'un_link'
export const CLEAR_LINKS = 'clear_links'

export const link = (item)=>{
  return{
    item: item,
    type: LINK
  }
}

export const clearlinks = (id)=>{
  return{
    type: CLEAR_LINKS
  }
}

export const unlink = (item)=>{
  return{
    item: item,
    type: UN_LINK
  }
}


export const createListing = (layer,service,type) => async dispatch => {
   var url=api.url+"/link"
   axios.post(url,{"layer":layer.key,"service":service.key})
  .then(response => {
    if(type=="layer"){
      dispatch(link(service))
      dispatch(getLinkedServices(layer.key))
    }
    if(type=="service"){
      dispatch(link(layer))
      dispatch(getLinkedLayers(service.key))
    }
    }
    )
	.catch(error => {
		   dispatch(axiosError(error))
	 })
}

export const removeListing = (layer,service,type) => async dispatch => {
   var url=api.url+"/unlink"
   axios.post(url,{"layer":layer.key,"service":service.key})
  .then(response => {
    if(type==="layer"){
      dispatch(unlink(service))
      dispatch(getLinkedServices(layer.key))
    }
    if(type==="service"){
      dispatch(unlink(layer))
      dispatch(getLinkedLayers(service.key))
    }
    })
		.catch(error => {
        console.log("ERROR",error)
		    dispatch(axiosError(error))
		 })
}









export const message = (message)=>{
  return{
    message: message,
    type: SHOW_MESSAGE
  }
}

export const selectlayer = (id) => {
  return {
    id: id,
    type: SELECT_LAYER
  }
}

export const deselectlayer = (id) => {
  return {
    id: id,
    type: DESELECT_LAYER
  }
}

export const subscribed = (id) => {
  return {
    id: id,
    type: SUBSCRIBE
  }
}

export const subscribe = (id) => async dispatch => {
  console.log("ID sub",id)
   var url=api.url+"/subscribe"
   axios.post(url,{"layer":id})
  .then(() => dispatch(subscribed(id)))
	.catch(error => {
	    dispatch(axiosError(error))
	 })
}


export const unsubscribed = (id) => {
  return {
    id: id,
    type: UNSUBSCRIBE
  }
}

export const unsubscribe = (id) => async dispatch => {
  console.log("ID unsub",id)
   var url=api.url+"/unsubscribe"
   axios.post(url,{"layer":id})
  .then(() => dispatch(unsubscribed(id)))
	.catch(error => {
	    dispatch(axiosError(error))
	 })
}

export const startscan = () => {
  return {
    type: SCANNING
  }
}
export const finishscan = () => {
  return {
    type: NOTSCANNING
  }
}

export const set_position = (pos) => {
  return {
    type: SET_POSITION,
    position: pos,
  }
}

export const receiveData = (json,pos) =>{
	return {
		type: RECEIVE_DATA,
		services: json.myServices,//.data.children.map(child => child.data),
    nearbyLayers: json.nearByLayers,
    myLayers: json.myLayers,
    position:pos
	}
}

export const autouserscan = (lat,long) => async dispatch => {
         dispatch(set_position([lat,long]))
         var url=api.url+"/userscan?lat="+lat+"&long="+long
         axios.get(url)
         .then(response => dispatch(receiveData(response.data,[lat,long])))
         .catch(error=>{dispatch(axiosError(error))
           dispatch(finishscan())
         });
}

export const userscan = () => async dispatch => {
    console.log("Logging")
    dispatch(startscan())
    navigator.geolocation.getCurrentPosition(
       (position) => {
         console.log("GOT LOCATION",position)
         let lat=position.coords.latitude
         let long=position.coords.longitude
         //COMMENT THIS OUT
         //let lat=52.19500
         //let long=0.117000
         dispatch(set_position([lat,long]))
         var url=api.url+"/userscan?lat="+lat+"&long="+long
         axios.get(url)
         .then(dispatch(finishscan()))
         .then(response => dispatch(receiveData(response.data,[lat,long])))
         .catch(error=>{dispatch(axiosError(error))
           dispatch(finishscan())
         });
        },
       (error) => {
         console.log(error.response)
         //dispatch(message("GPS Request Failed"))
         dispatch(finishscan())
       },
       { enableHighAccuracy: false, timeout: 4000, maximumAge: 1000 }
    )
}

export const axiosError=(error)=>dispatch =>{
 if (error.response) {
   console.log(error.response.data);
   console.log(error.response.status);
   console.log(error.response.headers);
   dispatch(message("Request Failed (Server error)"))
 } else if (error.request) {
   console.log(error.request);
   dispatch(message("Request Failed (No response)"))
 } else {
   console.log('Error', error.message);
   dispatch(message("Request Failed (Could not send)"))
 }
 console.log(error.config);
}

export const autopublicscan = (selected,lat,long) => async dispatch => {
   dispatch(set_position([lat,long]))
   var url=api.url+"/publicscan?lat="+lat+"&long="+long+"&selected=["+selected+"]"
   axios({
      method: 'get',
      url: url,
      timeout: 2000, // Let's say you want to wait at least 180 seconds
    })
    .then(response => dispatch(receiveData(response.data,[lat,long])))
    .catch(error=>{dispatch(axiosError(error))});
}

export const publicscan = (selected) => async dispatch => {
    console.log("Logging",selected)
    dispatch(startscan())
    navigator.geolocation.getCurrentPosition(
       (position) => {
         console.log("GOT LOCATION",position)
         let lat=position.coords.latitude
         let long=position.coords.longitude
         //COMMENT THIS OUT
         //let lat=52.19500
         //let long=0.117000
         dispatch(set_position([lat,long]))
         var url=api.url+"/publicscan?lat="+lat+"&long="+long+"&selected=["+selected+"]"
         axios({
            method: 'get',
            url: url,
            timeout: 2000, // Let's say you want to wait at least 180 seconds
          })
         .then(dispatch(finishscan()))
           .then(response => dispatch(receiveData(response.data,[lat,long])))
           .catch(error=>{dispatch(axiosError(error))
             dispatch(finishscan())
           });
          },
         (error) => {
           console.log(error.response)
           //dispatch(message("Request Failed (GPS error)"))
           dispatch(finishscan())
         },
       { enableHighAccuracy: false, timeout: 4000, maximumAge: 1000 }
    )
}


export const GET_LINKED_LAYERS='get_linked_layers'

export const receiveLinkedLayers = (layers)=>{
  return{
    layers: layers.data,
    type: GET_LINKED_LAYERS
  }
}

export const getLinkedLayers=(service) => async dispatch => {
   var url=api.url+"/linkedlayers"
   axios.post(url,{"service":service})
  .then(layers => dispatch(receiveLinkedLayers(layers)))
		.catch(error => {
		    dispatch(axiosError(error))
		 })
}

export const createService=(name,description,lat,long,live,link) => async dispatch => {
   var url=api.url+"/createservice"
   axios.post(url,{"name":name,"description":description,"lat":lat,"long":long,"live":live,"link":link})
  .then(response => dispatch(getMyServices()))
	.catch(error => {
	    dispatch(axiosError(error))
	 })
}

export const deleteService=(id) => async dispatch => {
   var url=api.url+"/removeservice"
   axios.post(url,{"service":id})
  .then(response => dispatch(getMyServices()))
		.catch(error => {
		    dispatch(axiosError(error))
		 })
}

export const updateService=(id,name,description) => async dispatch => {
   var url=api.url+"/updateservice"
   axios.post(url,{"id":id,"name":name,"description":description})
  .then(response => dispatch(getMyServices()))
		.catch(error => {
		    dispatch(axiosError(error))
		 })
}

export const receiveMyServices = (services)=>{
  console.log("services",services.data)
  return{
    services: services.data,
    type: GET_MY_SERVICES
  }
}

export const getMyServices = (id) => async dispatch => {
   var url=api.url+"/myservices"
   axios.get(url)
  .then(response => dispatch(receiveMyServices(response)))
	.catch(error => {
	    dispatch(axiosError(error))
	 })
}
