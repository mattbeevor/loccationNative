import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import Colours from '../constants/Colours';
import ServiceScreen from '../screens/ServiceScreen';
import LayerScreen from '../screens/LayerScreen';
import AuthScreen from '../screens/AuthScreen';

const Publicnav = createMaterialTopTabNavigator(
  {
    Services: {
      screen: ServiceScreen
    },
    Layers: {
      screen: LayerScreen,
    },
    Login: {
      screen: AuthScreen,
    }
  },
  {
    animationEnabled: true,
    swipeEnabled: true,
    tabBarOptions: {
      pressColor: Colours.tabBar,
      inactiveTintColor: Colours.darkTintColor,
      activeTintColor: Colours.tintColor,
      indicatorStyle:{
        backgroundColor: Colours.darkTintColor,
      },
      style: {
        backgroundColor: Colours.tabBar,
      },
    }
  }
);



export default Publicnav;
