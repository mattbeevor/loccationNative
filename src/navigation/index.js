import React, { Component,  } from 'react';
import Colours from '../constants/Colours';
import Publicnav from './publicNav';
import Usernav from './userNav';

import * as actions from '../actions';

import { connect } from 'react-redux';


class TabNav extends Component {

	componentDidMount(){
		navigator.geolocation.watchPosition(
      (position) => {
				if(this.props.logged_in){
					this.props.autouserscan(position.coords.latitude,position.coords.longitude)
				}else{
					let selected=this.props.my_layers.map(layer=>layer.key)
					this.props.autopublicscan(selected,position.coords.latitude,position.coords.longitude)
				}
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 5000, distanceFilter: 1 },
		);

	}


	render () {
    let nav
    if(this.props.logged_in){
      return <Usernav/>
    }else{
       return <Publicnav/>
    }

	}
};

const mapStateToProps = (state) => {
	return {
		my_layers: state.ServiceReducer.my_layers,
    logged_in: state.AuthReducer.logged_in
	};
}


export default connect(mapStateToProps,actions)(TabNav);
