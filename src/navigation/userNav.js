import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import Colours from '../constants/Colours';
import ServiceScreen from '../screens/ServiceScreen';
import LayerScreen from '../screens/LayerScreen';
import AuthScreen from '../screens/AuthScreen';
import MenuStack from '../screens/MenuStack';

const Usernav = createMaterialTopTabNavigator(
  {
    Services: {
      screen: ServiceScreen
    },
    Layers: {
      screen: LayerScreen,
    },
    Menu: {
      screen: MenuStack,
    }
  },
  {
    animationEnabled: true,
    swipeEnabled: true,
    tabBarOptions: {
      pressColor: Colours.tabBar,
      inactiveTintColor: Colours.darkTintColor,
      activeTintColor: Colours.tintColor,
      indicatorStyle:{
        backgroundColor: Colours.darkTintColor,
      },
      style: {
        backgroundColor: Colours.tabBar,
      },
    }
  }
);



export default Usernav;
