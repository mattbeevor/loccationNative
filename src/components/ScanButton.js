import React, { Component } from 'react';
import {View, StyleSheet, Text, TouchableHighlight} from 'react-native';
import Colours from '../constants/Colours';
import store from '../store';
import { userscan,publicscan } from '../actions';


class ScanButton extends Component {

  componentDidMount(){
    this.doscan()
  }


  doscan = () => {
    state=store.getState()
    if(state.AuthReducer.logged_in){
      store.dispatch(userscan())
    }else{
      selected=state.ServiceReducer.my_layers.map(layer=>layer.key)
      store.dispatch(publicscan(selected))
    }
  }

  render() {
    return (
      <View >
        <TouchableHighlight style={styles.buttoncontainer}>
        <Text>Pull down to scan</Text>
        </TouchableHighlight>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  buttoncontainer:{
    alignItems:'center',
    height: 30,
  },
});

export default ScanButton;
