import React, { Component } from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

import { connect } from 'react-redux';
import * as actions from '../actions';


class Login extends Component {

  constructor(props) {
    super(props);
    this.state = { email: "",
    password: ""
   };
  }

  login = () => {
    this.props.login(this.state.email)
  }
  logout = () => {
    this.props.logout()
  }

  render() {

    const isLoggedIn = this.props.logged_in;
    let button;

    if (isLoggedIn) {
      button = <View >
      <TouchableOpacity style={styles.Button}
      onPress={this.logout} >
      <Text>Log Out</Text>
      </TouchableOpacity>
      </View >
    } else {
      button =<View >
      <FormLabel>Email</FormLabel>
      <FormInput onChangeText={(text) => this.setState({email:text})}/>
      <FormValidationMessage>Error message</FormValidationMessage>
      <TouchableOpacity style={styles.Button}
      onPress={this.login} >
      <Text style={styles.text}>Log In</Text>
      </TouchableOpacity>
      </View>

    }

    return (


      <View>
        {button}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Button: {
    height:50,
    borderBottomWidth:1,
    borderBottomColor: "#AAAAAA",
    backgroundColor:"#EEEEFF"
  },
  text:{
    fontSize:25,
  }
});

const mapStateToProps = (state) => {
	return {
    logged_in: state.AuthReducer.logged_in,
	};
}

export default connect(mapStateToProps, actions)(Login);
