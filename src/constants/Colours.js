const tintColor = '#EFEEEE';
const darkTintColor = '#303040';

//s
export default {
  tintColor,
  darkTintColor,
  buttonColour: '#d89d6a',
  barColour: '#969494',
  tabBar: '#969494',

  tabIconDefault: '#888',
  iconGrey: '#505050',
  tabIconSelected: tintColor,
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
//https://coolors.co/96adc8-d7ffab-fcff6c-d89d6a-6d454c
